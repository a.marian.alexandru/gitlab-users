import React from 'react';
import styled from 'styled-components';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import UsersList from './pages/UsersList';
import User from './pages/User';

const Body = styled.div`
    max-width: 1000px;
    margin: 0 auto;
`;

function App() {
    return (
        <Body>
            <Router>
                <Switch>
                    <Route path="/users/:userId" component={User} />
                    <Route component={UsersList} />
                </Switch>
            </Router>
        </Body>
    );
}

export default App;
