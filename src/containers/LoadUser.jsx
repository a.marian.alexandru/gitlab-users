import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { token } from '../config';

const callAPI = url =>
    fetch(`https://gitlab.com/api/v4${url}`, {
        headers: {
            'Private-Token': token
        }
    });

const LoadUser = props => {
    const [user, setUser] = useState({});
    const [projects, setProjects] = useState([]);

    useEffect(
        () => {
            (async () => {
                const [user, projects] = await Promise.all([
                    callAPI(`/users/${props.id}`),
                    callAPI(`/users/${props.id}/projects`)
                ]);

                setUser(await user.json());
                setProjects(await projects.json());
            })();
        },
        // reload only if the username has changed
        [props.id]
    );

    return props.children(user, projects);
};

LoadUser.propTypes = {
    id: PropTypes.string,
    children: PropTypes.func.isRequired
};

export default LoadUser;
