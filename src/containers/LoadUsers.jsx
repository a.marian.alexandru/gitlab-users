import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { token } from '../config';

const LoadUsers = props => {
    const [users, setUsers] = useState([]);

    useEffect(
        () => {
            (async () => {
                const params = props.username
                    ? `?search=${props.username}`
                    : '';
                const response = await fetch(
                    `https://gitlab.com/api/v4/users${params}`,
                    {
                        headers: {
                            'Private-Token': token
                        }
                    }
                );

                setUsers(await response.json());
            })();
        },
        // reload only if the username has changed
        [props.username]
    );

    return props.children(users);
};

LoadUsers.propTypes = {
    username: PropTypes.string,
    children: PropTypes.func.isRequired
};

export default LoadUsers;
