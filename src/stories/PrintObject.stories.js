import React from 'react';
import PrintObject from '../components/PrintObject';

export default {
    title: 'PrintObject',
    component: PrintObject
};

export const noAvatar = () => (
    <PrintObject
        data={{
            id: '1',
            avatar_url: null,
            name: 'Marian',
            company: 'abc',
            description: 'Developer'
        }}
    />
);

export const withAvatar = () => (
    <PrintObject
        data={{
            id: '1',
            avatar_url: 'https://secure.gravatar.com/avatar/852e6b0de33876b0e5562ffeb3d95ee5?s=80&d=identicon',
            name: 'Chris',
            company: 'abc',
            description: 'BA'
        }}
    />
);
