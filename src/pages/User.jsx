import React from 'react';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import LoadUser from '../containers/LoadUser';
import PrintObject from '../components/PrintObject';

const Container = styled.div`
    display: flex;
    justify-content: space-between;
`;

const ProjectContainer = styled.div`
    margin-bottom: 20px;
`;

const UserProfile = props => <PrintObject data={props.user}/>;

const projectKeys = ['name', 'description', 'web_url', 'star_count'];
const Projects = props => {
    return (
        <ProjectContainer>
            <h3>Projects:</h3>
            {props.projects.map(project => (
                <PrintObject data={project} bodyKeys={projectKeys} />
            ))}
        </ProjectContainer>
    );
};

const User = props => {
    const { userId } = useParams();

    return (
        <LoadUser id={userId}>
            {(user, projects) => {
                if (!Object.keys(user).length) {
                    return 'Loading user...';
                }

                return (
                    <Container>
                        <UserProfile user={user} />
                        <Projects projects={projects} />
                    </Container>
                );
            }}
        </LoadUser>
    );
};

export default User;
