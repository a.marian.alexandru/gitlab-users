import React, { useState } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import LoadUsers from '../../containers/LoadUsers';
import Profile from '../../components/Profile';
import ProfileContainer from '../../components/ProfileContainer';
import Input from '../../components/Input';

const FormContainer = styled.div`
    margin-top: 20px;

    label {
        margin-left: 10px;
    }
`;

const UsersList = () => {
    const [username, setUsername] = useState(null);
    // debounce the event to prevent spam on API
    let timeout;
    const handleChange = event => {
        const username = event.target.value;
        clearTimeout(timeout);
        timeout = setTimeout(() => setUsername(username), 300);
    };

    return (
        <FormContainer>
            <label>Username:</label>{' '}
            <Input type="text" name="username" onChange={handleChange} />
            <LoadUsers username={username}>
                {users => (
                    <ProfileContainer>
                        {users.map(user => (
                            <Link to={`/users/${user.id}`}>
                                <Profile key={user.id} user={user} />
                            </Link>
                        ))}
                    </ProfileContainer>
                )}
            </LoadUsers>
        </FormContainer>
    );
};

export default UsersList;
