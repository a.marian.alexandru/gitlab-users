import React from 'react';
import { render, mount } from 'enzyme';
import UsersList from './index';

jest.useFakeTimers();

describe('<UsersList />', () => {
    let mockSuccessResponse = [];
    const mockFetchPromise = Promise.resolve({
        json: () => Promise.resolve(mockSuccessResponse)
    });

    beforeAll(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);
    });

    beforeEach(() => {
        mockSuccessResponse = [];
    });

    it('should show an empty list of users', () => {
        const userLists = render(<UsersList />);

        expect(userLists).toMatchSnapshot();
    });

    it('should render 3 users', () => {
        mockSuccessResponse = [
            {
                id: '1',
                avatar_url: null,
                name: 'marian',
                username: 'username'
            },
            {
                id: '2',
                avatar_url: 'google.com',
                name: 'joe',
                username: 'username'
            },
            {
                id: '3',
                avatar_url: 'amazon.com',
                name: 'george',
                username: 'username'
            }
        ];
        const userLists = render(<UsersList />);

        expect(userLists).toMatchSnapshot();
    });

    it('should call the API with the username from input', async () => {
        const userLists = mount(<UsersList />);
        userLists
            .find('[name="username"]')
            .first()
            .simulate('change', { target: { value: 'dummy-username' } });

        // because of the debounce
        jest.runAllTimers();

        expect(global.fetch).toHaveBeenCalledTimes(2);
        expect(global.fetch).toHaveBeenLastCalledWith(
            'https://gitlab.com/api/v4/users?search=dummy-username',
            expect.anything()
        );
    });
});
