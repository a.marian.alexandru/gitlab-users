import styled from 'styled-components';

export default styled.input`
    border: none;
    border-bottom: 1px solid #777;
    padding: 5px;
    width: 200px;
    outline: none;
    font-size: 14px;
`;
