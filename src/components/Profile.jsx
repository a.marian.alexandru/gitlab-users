import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    margin: 10px;
    width: 100px;
    cursor: pointer;

    img {
        width: 100%;
    }

    div {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
`;

const Profile = props => {
    const { user } = props;

    return (
        <Container>
            <img src={user.avatar_url} alt={user.username} />
            <div>{user.name}</div>
        </Container>
    );
};

export default Profile;
