import styled from 'styled-components';

export default styled.div`
    margin-top: 20px;
    display: flex;
    flex-wrap: wrap;
    justify-content: left;
    align-items: flex-start;
`;
