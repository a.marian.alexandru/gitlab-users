import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Input from '../components/Input';

const Item = styled.div`
    margin: 10px 0;

    label {
        width: 100px;
        display: inline-block;
        text-transform: capitalize;
    }
`;

const PrintObject = props => {
    const { id, avatar_url, ...rest } = props.data;
    const bodyKeys = props.bodyKeys || Object.keys(rest);

    return (
        <div>
            {avatar_url && <img src={avatar_url} alt={id} />}

            {bodyKeys.map(key => (
                <Item key={key}>
                    <label>{key.replace('_', ' ')}:</label>
                    <Input value={props.data[key] || ''} readOnly />
                </Item>
            ))}
        </div>
    );
};

PrintObject.propTypes = {
    data: PropTypes.object.isRequired,
    bodyKeys: PropTypes.arrayOf(String)
};

export default PrintObject;
