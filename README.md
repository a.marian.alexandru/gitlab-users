# gitlab-users

## Design patterns used

### Render props

Even if overkill for such a small projects, [render props](https://reactjs.org/docs/render-props.html) pattern is a great option for sharing code between components.
Here it was used for having very specific components that only load the data from API.

### React hooks

The latest and greatest pattern from React team, [hooks](https://reactjs.org/docs/hooks-overview.html) provide an great way of trimming the components and re-using logic.

## Mentions

### Styled components

I tried to keep the CSS at minimum and rather focus more on the design patterns, showing complex testing scenarios and having the code as clean as possible.
This being said...
Styled components work so well with React! My philosophy is to keep components as isolated as possible and to make them rather react to properties and not out-of-nowhere `!important` on classes. 

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run storybook`

Launches the [storybook](https://storybook.js.org/) framework for presenting and documenting the components
